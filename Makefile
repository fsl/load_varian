include ${FSLCONFDIR}/default.mk

PROJNAME = load_varian


XFILES    = load_varian_exe set_hdr_info get_orient get_dim
SCRIPTS   = load_varian
TCLFILES  = *.tcl
DATAFILES = petables *.scale
RUNTCLS   = LoadVarian MRinfo
LIBS      = -lfsl-miscmaths -lfsl-cprob -lfsl-utils -lfsl-NewNifti \
            -lfsl-znz -lfftw3

LVOBJS = main.o process.o hfunc.o phasefunc.o procfunc.o read_varian.o \
         image_ft.o scaleslice.o pssreorder.o segment.o byteorder.o \
         reslice.o kspace_mask.o pocs.o hermit.o margosian.o image_fftw.o \
         tep.o dropouts.o fermi.o entghost.o navigator.o resamp.o \
         concat_info.o fdf.o

all: ${XFILES}

load_varian_exe: ${LVOBJS}
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

set_hdr_info: set_hdr_info.o hfunc.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

get_orient: get_orient.o hfunc.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

get_dim: get_dim.o byteorder.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
