int hks_margosian(float* data,int ppl,int lpi,int ipv);
int hks_margosian_phase(float* data,float* phase,int ppl,int lpi,int *partial_lpi,int ipv);
int hks_margosian_hanning(float* data,int ppl,int lpi,int partial_lpi,int ipv);
int hks_margosian_correct(float* data,float* phase,int ppl,int lpi,int ipv);
int hks_margosian_zero(float* data,int ppl,int lpi,int ipv);
