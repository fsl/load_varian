void epi_phasecor(float *data,float *ref,int ppl,int lpi,int ipv);
void buono_cor(float *data,int ppl,int lpi,int ipv,int constrain);
void buono(float *data,float *cal,int ppl,int lpi,int constrain);
void buono_calib(float *data,float *cal,int ppl,int lpi,int ipv,int constrain);
void buono_apply(float *data,float *cal,int ppl,int lpi,int ipv);
void buono_correct(float *data,float* cal,int ppl,int lpi);
void fix_cal_file(float *cal,int ppl,int ipv);
int calc_full_ref(float *ref,int ppl,int lpi,int ipv,int seg);
int calc_phase_cor(float *ref,int ppl,int lpi,int ipv,int nseg,int order,int* cenline);
void linfit(float *phs1,float *phs2,float *mod,int ppl);
void linear_regression(float *phs,float *mod,int ppl,float *a,float *b);
void unwrap(float *data,int ppl);
void unwrap2(float *data,int ppl);
void polyfit(float* phs1,float* phs2,float* mod1,int ppl);
void find_coef(float* dif,float* wt,int ppl,float* cof);
float poly5fit(float p[]);
int hu_cal(float *data,float *ref,int ppl,int lpi,int ipv,int constrain);
void hu_cor(float *data,float *ref,int ppl,int lpi,int ipv);
void create_ref_scan(float *ref,int ppl,int lpi,int ipv,int nseg,float ph0,float ph1);
void phase_rot_init(float *data,float *ref,long volsize);
void phase_rot(float *data,float *ref,long volsize);
void smodulus_init(float *data,float *ref,long volsize);
void smodulus(float *data,float *ref,long volsize);
void polynomial_fit(float *data,float *wt,int n,float* cof,int ncof);
