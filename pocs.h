int hks_pocs(float* data,int ppl,int lpi,int ipv);
int hks_pocs_phase(float* data,float* phase,int ppl,int lpi,int *partial_lpi,int ipv);
int hks_pocs_imgen(float* data,float* phase,float* pseudo,int ppl,int lpi,int ipv);
int hks_pocs_merge(float* data,float* pseudo,int ppl,int lpi,int partial_lpi,int ipv);
